<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! String action;%>
<%--@elvariable id="airport" type="model.Airport"--%>
<html>
<head>
    <title>Airport Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <c:if test="${airport != null}">
    <form method="get">
        <%action = "Edit Airport";%>
        </c:if>
        <c:if test="${airport == null}">
        <form method="post">
            <%action = "Add new Airport";%>
            </c:if>
            <table class="addTable">
                <caption><h2><%=action%></h2></caption>
                <input type="hidden" name="id" size="30" value="<c:out value='${airport.id}' />"/>
                <tr>
                    <th>AirportName:</th>
                    <td>
                        <label>
                            <input type="text" name="airportName" required size="30" value="<c:out value='${airport.name}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>AirportCity:</th>
                    <td>
                        <label>
                            <input type="text" name="airportCity" required size="30" value="<c:out value='${airport.city}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>AirportCountry:</th>
                    <td>
                        <label>
                            <input type="text" name="airportCountry" required size="30"
                                   value="<c:out value='${airport.country}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>AirportCode:</th>
                    <td>
                        <label>
                            <input type="text" name="airportCode" required size="30" value="<c:out value='${airport.code}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
