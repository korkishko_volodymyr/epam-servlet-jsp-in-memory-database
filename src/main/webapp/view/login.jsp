<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--@elvariable id="message" type="java.lang.String"--%>
<html>
<head>
    <title>Airport Management</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form method="post">
        <table class="addTable">
            <caption><h2>Login as admin*</h2></caption>
            <tr>
                <th>Login:</th>
                <td>
                    <label>
                        <input type="text" name="login" required size="20"/>
                    </label>
                </td>
            </tr>
            <tr>
                <th>Password:</th>
                <td>
                    <label>
                        <input type="password" min="1" name="password" required/>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Login" class="addButton"/>
                </td>
            </tr>
        </table>
        <h4 style="text-align: center"><i>*to login read readme.md</i></h4>
        <c:if test="${message != null}">
            <div style="text-align: center;color: #ff3333">
                <h3>${message}</h3>
            </div>
        </c:if>
    </form>
</div>
</body>
</html>
