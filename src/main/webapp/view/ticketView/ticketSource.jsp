<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%! String action;%>
<%--@elvariable id="ticket" type="model.Ticket"--%>
<c:if test="${ticket != null}">
    <%action = "Edit Ticket";%>
</c:if>
<c:if test="${ticket == null}">
    <%action = "Add new Ticket";%>
</c:if>
<caption><h2><%=action%></h2></caption>
<input type="hidden" name="id" size="30" value="<c:out value='${ticket.id}' />"/>
<tr>
    <th>Number:</th>
    <td>
        <label>
            <input type="text" name="number" required size="30" value="<c:out value='${ticket.number}' />"/>
        </label>
    </td>
</tr>
<tr>
    <th>Source:</th>
    <td>
        <label>
            <input type="text" name="source" required size="30" value="<c:out value='${ticket.source}' />"/>
        </label>
    </td>
</tr>
<tr>
    <th>Destination:</th>
    <td>
        <label>
            <input type="text" name="destination" required size="30"
                   value="<c:out value='${ticket.destination}' />"/>
        </label>
    </td>
</tr>
<tr>
    <th>DateOfFlight:</th>
    <td>
        <label>
            <input type="datetime-local" name="dateOfFlight" required size="30"
                   value="<c:out value='${ticket.dateOfFlight}' />"/>
        </label>
    </td>
</tr>
<tr>
    <th>FlightDuration(min):</th>
    <td>
        <label>
            <input type="number" name="flightDuration" min="1"
                   value="<c:out value='${ticket.flightDuration}' />"/>
        </label>
    </td>
</tr>
<tr>
    <th>ServiceClass:</th>
    <td>
        <label>
            <select name="serviceClass">
                <option value="ECONOMY">ECONOMY</option>
                <option value="BUSINESS">BUSINESS</option>
                <option value="COMFORT">COMFORT</option>
            </select>
        </label>
    </td>
</tr>
<tr>
    <th>Price(USD):</th>
    <td>
        <label>
            <input type="number" name="price" min="0" required size="30" value="<c:out value='${ticket.price}' />"/>
        </label>
    </td>
</tr>