<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airport</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="mainTable">
        <caption><h2>List of Tickets</h2></caption>
        <tr>
            <th>Number</th>
            <th>Source</th>
            <th>Destination</th>
            <th>DateOfFlight</th>
            <th>FlightDuration(min.)</th>
            <th>ServiceClass</th>
            <th>Price</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Passport</th>
            <th>Action</th>
        </tr>
        <c:forEach var="ticket" items="${requestScope.tickets}">
            <tr>
                <td><c:out value="${ticket.number}"/></td>
                <td><c:out value="${ticket.source}"/></td>
                <td><c:out value="${ticket.destination}"/></td>
                <td><c:out value="${ticket.dateOfFlight}"/></td>
                <td><c:out value="${ticket.flightDuration}"/></td>
                <td><c:out value="${ticket.serviceClass}"/></td>
                <td><c:out value="${ticket.price}"/>$</td>
                <td><c:out value="${ticket.passenger.firstName}"/></td>
                <td><c:out value="${ticket.passenger.lastName}"/></td>
                <td><c:out value="${ticket.passenger.passportNumber}"/></td>
                <td>
                    <div style="margin-left: 15%">
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/editTicket?id=<c:out value='${ticket.id}'/>"
                                  method="post">
                                <a href="#" onclick="this.parentNode.submit()" class="editButton">Edit</a>
                            </form>
                        </div>
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/ticket?id=<c:out value='${ticket.id}'/>"
                                  method="post">
                                <a href="#" onclick="this.parentNode.submit()" class="deleteButton">Delete</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/ticketAction"/>" class="addButton">
            Add new Ticket
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
        <a href="<c:url value="/airline"/>" class="nextButton">
            Go to Airlines
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>
