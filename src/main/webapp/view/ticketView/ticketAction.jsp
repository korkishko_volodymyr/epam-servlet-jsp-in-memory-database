<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--@elvariable id="ticket" type="model.Ticket"--%>
<html>
<head>
    <title>Ticket Page</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <c:if test="${ticket == null}">
        <form method="post">
            <table class="addTable">
                <jsp:include page="ticketSource.jsp"/>
                <tr>
                    <th>Passenger:</th>
                    <td>
                        <label>
                            <select name="passenger" required>
                                <option value="" disabled selected hidden>Select passenger</option>
                                <c:forEach items="${requestScope.passengersList}" var="passenger">
                                    <option value="${passenger.id}">${passenger.firstName}  ${passenger.lastName}</option>
                                </c:forEach>
                            </select>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Airline:</th>
                    <td>
                        <label>
                            <select name="airline" required>
                                <option value="" disabled selected hidden>Select airline</option>
                                <c:forEach items="${requestScope.airlinesList}" var="airline">
                                    <option value="${airline.id}">${airline.name}</option>
                                </c:forEach>
                            </select>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Airport:</th>
                    <td>
                        <label>
                            <select name="airport" required>
                                <option value="" disabled selected hidden>Select airport</option>
                                <c:forEach items="${requestScope.airportsList}" var="airport">
                                    <option value="${airport.id}">${airport.name}</option>
                                </c:forEach>
                            </select>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Plane:</th>
                    <td>
                        <label>
                            <select name="plane" required>
                                <option value="" disabled selected hidden>Select plane</option>
                                <c:forEach items="${requestScope.planesList}" var="plane">
                                    <option value="${plane.id}">${plane.model}</option>
                                </c:forEach>
                            </select>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
    </c:if>
    <c:if test="${ticket != null}">
        <form method="get">
            <table class="addTable">
                <jsp:include page="ticketSource.jsp"/>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
    </c:if>
</div>
</body>
</html>
