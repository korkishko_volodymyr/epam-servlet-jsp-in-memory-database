<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! String action;%>
<%--@elvariable id="plane" type="model.Plane"--%>
<html>
<head>
    <title>Plane Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <c:if test="${plane != null}">
    <form method="get">
        <%action = "Edit Plane";%>
        </c:if>
        <c:if test="${plane == null}">
        <form method="post">
            <%action = "Add new Plane";%>
            </c:if>
            <table class="addTable">
                <caption><h2><%=action%></h2></caption>
                <input type="hidden" name="id" size="30" value="<c:out value='${plane.id}' />"/>
                <tr>
                    <th>PlaneModel:</th>
                    <td>
                        <label>
                            <input type="text" name="planeModel" required size="30"
                                   value="<c:out value='${plane.model}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>PlanePlaces:</th>
                    <td>
                        <label>
                            <input type="number" min="1" name="planePlaces" required
                                   value="<c:out value='${plane.numbPlaces}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
