<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! String action;%>
<%--@elvariable id="passenger" type="model.Passenger"--%>
<html>
<head>
    <title>Passenger Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <c:if test="${passenger != null}">
    <form method="get">
        <%action = "Edit Passenger";%>
        </c:if>
        <c:if test="${passenger == null}">
        <form method="post">
            <%action = "Add new Passenger";%>
            </c:if>
            <table class="addTable">
                <caption><h2><%=action%></h2></caption>
                <input type="hidden" name="id" size="30" value="<c:out value='${passenger.id}' />"/>
                <tr>
                    <th>FirstName:</th>
                    <td>
                        <label>
                            <input type="text" name="firstName" required size="30"
                                   value="<c:out value='${passenger.firstName}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>LastName:</th>
                    <td>
                        <label>
                            <input type="text" name="lastName" required size="30"
                                   value="<c:out value='${passenger.lastName}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Age:</th>
                    <td>
                        <label>
                            <input type="number" min="1" name="age" required value="<c:out value='${passenger.age}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>PhoneNumber:</th>
                    <td>
                        <label>
                            <input type="text" name="phoneNumber" required size="30"
                                   value="<c:out value='${passenger.phoneNumber}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Sex:</th>
                    <td>
                        <label>
                            <select name="sex">
                                <option value="MALE">MALE</option>
                                <option value="FEMALE">FEMALE</option>
                            </select>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>PassportNumber:</th>
                    <td>
                        <label>
                            <input type="text" name="passportNumber" required size="30"
                                   value="<c:out value='${passenger.passportNumber}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
