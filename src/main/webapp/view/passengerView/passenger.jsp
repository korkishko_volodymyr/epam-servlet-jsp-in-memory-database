<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Passenger</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="mainTable">
        <caption><h2>List of Passengers</h2></caption>
        <tr>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Age</th>
            <th>PhoneNumber</th>
            <th>Sex</th>
            <th>PassportNumber</th>
            <th>Action</th>
        </tr>
        <c:forEach var="passenger" items="${requestScope.passengers}">
            <tr>
                <td><c:out value="${passenger.firstName}"/></td>
                <td><c:out value="${passenger.lastName}"/></td>
                <td><c:out value="${passenger.age}"/></td>
                <td><c:out value="${passenger.phoneNumber}"/></td>
                <td><c:out value="${passenger.sex}"/></td>
                <td><c:out value="${passenger.passportNumber}"/></td>
                <td>
                    <div style="margin-left: 28%">
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/editPassenger?id=<c:out value='${passenger.id}' />"
                                  method="post">
                                <a href="#"  onclick="this.parentNode.submit()" class="editButton">Edit</a>
                            </form>
                        </div>
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/passenger?id=<c:out value='${passenger.id}' />"
                                  method="post">
                                <a href="#"  onclick="this.parentNode.submit()" class="deleteButton">Delete</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/passengerAction"/>" class="addButton">
            Add new Passenger
        </a>
        <a href="<c:url value="/airline"/>" class="nextButton">
            Go to Airlines
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>
