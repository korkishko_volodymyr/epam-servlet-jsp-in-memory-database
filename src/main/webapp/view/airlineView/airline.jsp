<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airline</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="smallTable">
        <caption><h2>List of Airlines</h2></caption>
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Action</th>
        </tr>
        <c:forEach var="airline" items="${requestScope.airlines}">
            <tr>
                <td><c:out value="${airline.name}"/></td>
                <td><c:out value="${airline.code}"/></td>
                <td>
                    <div style="margin-left: 34%">
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/editAirline?id=<c:out value='${airline.id}' />"
                                  method="post">
                                <a href="#" onclick="this.parentNode.submit()" class="editButton">Edit</a>
                            </form>
                        </div>
                        <div style="float: left;">
                            <form action="${pageContext.request.contextPath}/airline?id=<c:out value='${airline.id}' />"
                                  method="post">
                                <a href="#" onclick="this.parentNode.submit()" class="deleteButton">Delete</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/airlineAction"/>" class="addButton">
            Add new Airline
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>
