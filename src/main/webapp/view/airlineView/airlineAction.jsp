<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! String action;%>
<%--@elvariable id="airline" type="model.Airline"--%>
<html>
<head>
    <title>Airline Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <c:if test="${airline != null}">
    <form method="get">
        <%action = "Edit Airline";%>
        </c:if>
        <c:if test="${airline == null}">
        <form method="post">
            <%action = "Add new Airline";%>
            </c:if>
            <table class="addTable">
                <caption><h2><%=action%></h2></caption>
                <input type="hidden" name="id" size="30" value="<c:out value='${airline.id}' />"/>
                <tr>
                    <th>Name:</th>
                    <td>
                        <label>
                            <input type="text" name="airlineName" required size="30" value="<c:out value='${airline.name}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>Code:</th>
                    <td>
                        <label>
                            <input type="text" name="airlineCode" required size="30" value="<c:out value='${airline.code}' />"/>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Save" class="addButton"/>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
