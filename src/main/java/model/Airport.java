package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
public class Airport {
    private static final int INITIAL_VALUE = 5;
    private static AtomicInteger atomicInteger = new AtomicInteger(INITIAL_VALUE); // auto_increment doesn't work
    private int id;
    private String name;
    private String city;
    private String country;
    private String code;

    public Airport(String name, String city, String country, String code) {
        this.id = atomicInteger.incrementAndGet();
        this.name = name;
        this.city = city;
        this.country = country;
        this.code = code;
    }
}
