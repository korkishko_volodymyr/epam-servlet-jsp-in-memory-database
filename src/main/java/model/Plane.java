package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
public class Plane {
    private static final int INITIAL_VALUE = 4;
    private static AtomicInteger atomicInteger = new AtomicInteger(INITIAL_VALUE); // auto_increment doesn't work
    private int id;
    private String model;
    private int numbPlaces;

    public Plane(String model, int numbPlaces) {
        this.id = atomicInteger.incrementAndGet();
        this.model = model;
        this.numbPlaces = numbPlaces;
    }
}
