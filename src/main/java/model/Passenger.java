package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import model.enums.Sex;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
public class Passenger {
    private static final int INITIAL_VALUE = 4;
    private static AtomicInteger atomicInteger = new AtomicInteger(INITIAL_VALUE); // auto_increment doesn't work
    private int id;
    private String firstName;
    private String lastName;
    private Sex sex;
    private int age;
    private String phoneNumber;
    private String passportNumber;

    public Passenger(String firstName, String lastName, Sex sex, int age, String phoneNumber, String passportNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.age = age;
        this.id = atomicInteger.incrementAndGet();
        this.phoneNumber = phoneNumber;
        this.passportNumber = passportNumber;
    }
}
