package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import model.enums.ServiceClass;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
@Builder
public class Ticket {
    private static final int INITIAL_VALUE = 5;
    private static AtomicInteger atomicInteger = new AtomicInteger(INITIAL_VALUE); // auto_increment doesn't work
    private int id;
    private String number;
    private String source;
    private String destination;
    private String dateOfFlight;
    private int flightDuration;
    private ServiceClass serviceClass;
    private int price;
    private Passenger passenger;
    private Airline airline;
    private Airport departureAirport;
    private Plane plane;

    public Ticket(String number, String source, String destination, String dateOfFlight, int flightDuration,
                  ServiceClass serviceClass, int price, Passenger passenger,
                  Airline airline, Airport departureAirport, Plane plane) {
        this.id = atomicInteger.incrementAndGet();
        this.number = number;
        this.source = source;
        this.destination = destination;
        this.dateOfFlight = dateOfFlight;
        this.flightDuration = flightDuration;
        this.serviceClass = serviceClass;
        this.price = price;
        this.passenger = passenger;
        this.airline = airline;
        this.departureAirport = departureAirport;
        this.plane = plane;
    }
}
