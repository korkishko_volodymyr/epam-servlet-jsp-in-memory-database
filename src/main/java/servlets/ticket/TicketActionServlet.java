package servlets.ticket;

import database.dao.contract.*;
import database.dao.impl.*;
import model.*;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TicketActionServlet", urlPatterns = "/ticketAction")
public class TicketActionServlet extends HttpServlet {

    private static final String PAGE = "/view/ticketView/ticketAction.jsp";
    private TicketDAO ticketDAO = new TicketDAOImpl();
    private AirlineDAO airlineDAO = new AirlineDAOImpl();
    private PassengerDAO passengerDAO = new PassengerDAOImpl();
    private PlaneDAO planeDAO = new PlaneDAOImpl();
    private AirportDAO airportDAO = new AirportDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Airline> availableAirlines = airlineDAO.getAll();
        List<Passenger> availablePassengers = passengerDAO.getAll();
        List<Airport> availableAirports = airportDAO.getAll();
        List<Plane> availablePlanes = planeDAO.getAll();
        req.setAttribute("airlinesList", availableAirlines);
        req.setAttribute("passengersList", availablePassengers);
        req.setAttribute("planesList", availablePlanes);
        req.setAttribute("airportsList", availableAirports);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Ticket newTicket = ModelUtil.getTicket(req);
        Airline airline = airlineDAO.getById(Integer.parseInt(req.getParameter("airline")));
        Passenger passenger = passengerDAO.getById(Integer.parseInt(req.getParameter("passenger")));
        Plane plane = planeDAO.getById(Integer.parseInt(req.getParameter("plane")));
        Airport airport = airportDAO.getById(Integer.parseInt(req.getParameter("airport")));
        newTicket.setAirline(airline);
        newTicket.setPassenger(passenger);
        newTicket.setDepartureAirport(airport);
        newTicket.setPlane(plane);
        ticketDAO.insert(newTicket);
        resp.sendRedirect("/ticket");
    }
}
