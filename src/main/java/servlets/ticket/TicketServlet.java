package servlets.ticket;

import database.dao.contract.TicketDAO;
import database.dao.impl.TicketDAOImpl;
import model.Ticket;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TicketServlet", urlPatterns = "/ticket")
public class TicketServlet extends HttpServlet {

    private static final String PAGE = "/view/ticketView/ticket.jsp";
    private TicketDAO ticketDAO = new TicketDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Ticket> ticketList = ticketDAO.getAll();
        req.setAttribute("tickets", ticketList);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        ticketDAO.delete(id);
        resp.sendRedirect("/ticket");
    }
}
