package servlets.ticket;

import database.dao.contract.TicketDAO;
import database.dao.impl.TicketDAOImpl;
import model.Ticket;
import model.enums.ServiceClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdateTicketServlet", urlPatterns = "/editTicket")
public class UpdateTicketServlet extends HttpServlet {

    private static final String PAGE = "/view/ticketView/ticketAction.jsp";
    private TicketDAO ticketDAO = new TicketDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        String number = req.getParameter("number");
        String source = req.getParameter("source");
        String dest = req.getParameter("destination");
        String date = req.getParameter("dateOfFlight");
        int flightDuration = Integer.parseInt(req.getParameter("flightDuration"));
        ServiceClass serviceClass = ServiceClass.valueOf(req.getParameter("serviceClass"));
        int price = Integer.parseInt(req.getParameter("price"));
        Ticket ticket = Ticket.builder()
                .id(id)
                .number(number)
                .source(source)
                .destination(dest)
                .dateOfFlight(date)
                .flightDuration(flightDuration)
                .serviceClass(serviceClass)
                .price(price)
                .build();
        ticketDAO.update(ticket);
        resp.sendRedirect("/ticket");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final int ticketId = Integer.parseInt(req.getParameter("id"));
        Ticket ticket = ticketDAO.getById(ticketId);
        req.setAttribute("ticket", ticket);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }
}
