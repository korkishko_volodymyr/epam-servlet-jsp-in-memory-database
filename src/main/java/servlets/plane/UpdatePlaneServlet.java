package servlets.plane;

import database.dao.impl.PlaneDAOImpl;
import database.dao.contract.PlaneDAO;
import model.Plane;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdatePlaneServlet", urlPatterns = "/editPlane")
public class UpdatePlaneServlet extends HttpServlet {

    private static final String PAGE = "/view/planeView/planeAction.jsp";
    private PlaneDAO planeDAO = new PlaneDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        Plane plane = ModelUtil.getPlane(req);
        plane.setId(id);
        planeDAO.update(plane);
        resp.sendRedirect("/plane");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final int planeId = Integer.parseInt(req.getParameter("id"));
        Plane plane = planeDAO.getById(planeId);
        req.setAttribute("plane", plane);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }
}
