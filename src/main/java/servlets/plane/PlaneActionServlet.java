package servlets.plane;

import database.dao.impl.PlaneDAOImpl;
import database.dao.contract.PlaneDAO;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PlaneActionServlet", urlPatterns = "/planeAction")
public class PlaneActionServlet extends HttpServlet {

    private static final String PAGE = "/view/planeView/planeAction.jsp";
    private PlaneDAO planeDAO = new PlaneDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        planeDAO.insert(ModelUtil.getPlane(req));
        resp.sendRedirect("/plane");
    }
}
