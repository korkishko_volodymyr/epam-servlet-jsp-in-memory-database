package servlets.plane;

import database.dao.impl.PlaneDAOImpl;
import database.dao.contract.PlaneDAO;
import model.Plane;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PlaneServlet", urlPatterns = "/plane")
public class PlaneServlet extends HttpServlet {

    private static final String PAGE = "/view/planeView/plane.jsp";
    private PlaneDAO planeDAO = new PlaneDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Plane> planeList = planeDAO.getAll();
        req.setAttribute("planes", planeList);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        planeDAO.delete(id);
        resp.sendRedirect("/plane");
    }
}
