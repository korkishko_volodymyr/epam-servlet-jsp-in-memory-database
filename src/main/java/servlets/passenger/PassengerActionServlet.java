package servlets.passenger;

import database.dao.impl.PassengerDAOImpl;
import database.dao.contract.PassengerDAO;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PassengerActionServlet", urlPatterns = "/passengerAction")
public class PassengerActionServlet extends HttpServlet {

    private static final String PAGE = "/view/passengerView/passengerAction.jsp";
    private PassengerDAO passengerDAO = new PassengerDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        passengerDAO.insert(ModelUtil.getPassenger(req));
        resp.sendRedirect("/passenger");
    }
}
