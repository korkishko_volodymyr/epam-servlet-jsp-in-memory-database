package servlets.passenger;

import database.dao.impl.PassengerDAOImpl;
import database.dao.contract.PassengerDAO;
import model.Passenger;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdatePassengerServlet", urlPatterns = "/editPassenger")
public class UpdatePassengerServlet extends HttpServlet {

    private static final String PAGE = "/view/passengerView/passengerAction.jsp";
    private PassengerDAO passengerDAO = new PassengerDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        Passenger passenger = ModelUtil.getPassenger(req);
        passenger.setId(id);
        passengerDAO.update(passenger);
        resp.sendRedirect("/passenger");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final int pasId = Integer.parseInt(req.getParameter("id"));
        Passenger passenger = passengerDAO.getById(pasId);
        req.setAttribute("passenger", passenger);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }
}
