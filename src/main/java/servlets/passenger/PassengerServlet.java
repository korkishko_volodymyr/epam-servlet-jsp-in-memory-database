package servlets.passenger;

import database.dao.impl.PassengerDAOImpl;
import database.dao.contract.PassengerDAO;
import model.Passenger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PassengerServlet", urlPatterns = "/passenger")
public class PassengerServlet extends HttpServlet {

    private static final String PAGE = "/view/passengerView/passenger.jsp";
    private PassengerDAO passengerDAO = new PassengerDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Passenger> passengers = passengerDAO.getAll();
        req.setAttribute("passengers", passengers);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        passengerDAO.delete(id);
        resp.sendRedirect("/passenger");
    }
}