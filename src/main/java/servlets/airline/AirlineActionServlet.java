package servlets.airline;

import database.dao.impl.AirlineDAOImpl;
import database.dao.contract.AirlineDAO;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AirlineActionServlet", urlPatterns = "/airlineAction")
public class AirlineActionServlet extends HttpServlet {

    private static final String PAGE = "/view/airlineView/airlineAction.jsp";
    private AirlineDAO airlineDAO = new AirlineDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        airlineDAO.insert(ModelUtil.getAirline(req));
        resp.sendRedirect("/airline");
    }
}
