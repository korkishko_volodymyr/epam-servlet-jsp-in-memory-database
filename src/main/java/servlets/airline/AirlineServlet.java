package servlets.airline;

import database.dao.impl.AirlineDAOImpl;
import database.dao.contract.AirlineDAO;
import model.Airline;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AirlineServlet", urlPatterns = "/airline")
public class AirlineServlet extends HttpServlet {

    private static final String PAGE = "/view/airlineView/airline.jsp";
    private AirlineDAO airlineDAO = new AirlineDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Airline> airlines = airlineDAO.getAll();
        req.setAttribute("airlines", airlines);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        airlineDAO.delete(id);
        resp.sendRedirect("/airline");
    }
}
