package servlets.airline;

import database.dao.impl.AirlineDAOImpl;
import database.dao.contract.AirlineDAO;
import model.Airline;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdateAirlineServlet", urlPatterns = "/editAirline")
public class UpdateAirlineServlet extends HttpServlet {

    private static final String PAGE = "/view/airlineView/airlineAction.jsp";
    private AirlineDAO airlineDAO = new AirlineDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        Airline airline = ModelUtil.getAirline(req);
        airline.setId(id);
        airlineDAO.update(airline);
        resp.sendRedirect("/airline");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final int airId = Integer.parseInt(req.getParameter("id"));
        Airline airline = airlineDAO.getById(airId);
        req.setAttribute("airline", airline);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }
}
