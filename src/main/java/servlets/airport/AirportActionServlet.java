package servlets.airport;

import database.dao.impl.AirportDAOImpl;
import database.dao.contract.AirportDAO;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AirportActionServlet", urlPatterns = "/airportAction")
public class AirportActionServlet extends HttpServlet {

    private static final String PAGE = "/view/airportView/airportAction.jsp";
    private AirportDAO airportDAO = new AirportDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        airportDAO.insert(ModelUtil.getAirport(req));
        resp.sendRedirect("/airport");
    }
}
