package servlets.airport;

import database.dao.impl.AirportDAOImpl;
import database.dao.contract.AirportDAO;
import model.Airport;
import util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdateAirportServlet", urlPatterns = "/editAirport")
public class UpdateAirportServlet extends HttpServlet {

    private static final String PAGE = "/view/airportView/airportAction.jsp";
    private AirportDAO airportDAO = new AirportDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        Airport airport = ModelUtil.getAirport(req);
        airport.setId(id);
        airportDAO.update(airport);
        resp.sendRedirect("/airport");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final int apId = Integer.parseInt(req.getParameter("id"));
        Airport airport = airportDAO.getById(apId);
        req.setAttribute("airport", airport);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }
}
