package servlets.airport;

import database.dao.impl.AirportDAOImpl;
import database.dao.contract.AirportDAO;
import model.Airport;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AirportServlet", urlPatterns = "/airport")
public class AirportServlet extends HttpServlet {

    private static final String PAGE = "/view/airportView/airport.jsp";
    private AirportDAO airportDAO = new AirportDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Airport> airports = airportDAO.getAll();
        req.setAttribute("airports", airports);
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final int id = Integer.parseInt(req.getParameter("id"));
        airportDAO.delete(id);
        resp.sendRedirect("/airport");
    }
}
