package servlets;

import model.exception.FailedLoginException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "LoginServlet", urlPatterns = "/")
public class LoginServlet extends HttpServlet {

    private static final String PAGE = "/view/login.jsp";
    private static final String LOGIN = "korkishko1998";
    private static final String PASSWORD = "epamLviv";
    private static final String INVALID_LOGIN_MESSAGE = "Wrong Login or Password!";
    private static final String LOGIN_SUCCESSFUL_MESSAGE = "Login successful";
    private static Logger logger = Logger.getLogger(LoginServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        try {
            login(login, password);
            logger.info(LOGIN_SUCCESSFUL_MESSAGE);
            resp.sendRedirect("/ticket");
        } catch (FailedLoginException e) {
            logger.warn("Login failed with login: " + login + " password: " + password, e);
            req.setAttribute("message", INVALID_LOGIN_MESSAGE);
            req.getRequestDispatcher(PAGE).forward(req, resp);
        }
    }

    private void login(String login, String password) throws FailedLoginException {
        if (!(login.equals(LOGIN) && password.equals(PASSWORD))) {
            throw new FailedLoginException("Login failed");
        }
    }
}
