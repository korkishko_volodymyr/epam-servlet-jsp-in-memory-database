package util;

import model.*;
import model.enums.ServiceClass;
import model.enums.Sex;

import javax.servlet.http.HttpServletRequest;

public class ModelUtil {

    public static Plane getPlane(HttpServletRequest req) {
        String planeModel = req.getParameter("planeModel");
        int places = Integer.parseInt(req.getParameter("planePlaces"));
        return new Plane(planeModel, places);
    }

    public static Passenger getPassenger(HttpServletRequest req) {
        String fName = req.getParameter("firstName");
        String lName = req.getParameter("lastName");
        Sex sex = Sex.valueOf(req.getParameter("sex"));
        int age = Integer.parseInt(req.getParameter("age"));
        String phoneNumber = req.getParameter("phoneNumber");
        String passport = req.getParameter("passportNumber");
        return new Passenger(fName, lName, sex, age, phoneNumber, passport);
    }

    public static Airline getAirline(HttpServletRequest req) {
        String name = req.getParameter("airlineName");
        String code = req.getParameter("airlineCode");
        return new Airline(name, code);
    }

    public static Airport getAirport(HttpServletRequest req) {
        String name = req.getParameter("airportName");
        String city = req.getParameter("airportCity");
        String country = req.getParameter("airportCountry");
        String code = req.getParameter("airportCode");
        return new Airport(name, city, country, code);
    }

    public static Ticket getTicket(HttpServletRequest req) {
        String number = req.getParameter("number");
        String source = req.getParameter("source");
        String dest = req.getParameter("destination");
        String date = req.getParameter("dateOfFlight");
        int flightDuration = Integer.parseInt(req.getParameter("flightDuration"));
        ServiceClass serviceClass = ServiceClass.valueOf(req.getParameter("serviceClass"));
        int price = Integer.parseInt(req.getParameter("price"));
        return new Ticket(number, source, dest, date, flightDuration, serviceClass, price,
                null, null, null, null);
    }
}
