package database.dao.impl;

import database.AirportDB;
import database.dao.contract.PlaneDAO;
import model.Plane;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlaneDAOImpl implements PlaneDAO {

    private static Logger logger = Logger.getLogger(PlaneDAOImpl.class.getName());
    private Connection connection = AirportDB.getInstance().getConnection();

    @Override
    public void insert(Plane plane) {
        String sql = "Insert into Plane values (? ,?, ?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, plane.getId());
            stmt.setString(2, plane.getModel());
            stmt.setInt(3, plane.getNumbPlaces());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot insert plane with id: " + plane.getId(), e);
        }
    }

    @Override
    public Plane getById(int id) {
        String sql = "Select * from Plane where planeId = ?";
        Plane plane = null;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            logger.info("Executing query: " + stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                plane = getPlaneFromResultSet(rs);
            }
        } catch (SQLException e) {
            logger.error("Cannot get plane with id: " + id, e);
        }
        return plane;
    }

    @Override
    public List<Plane> getAll() {
        List<Plane> planes = new ArrayList<>();
        String sql = "Select * from Plane";
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            logger.info("Executing query: " + sql);
            while (rs.next()) {
                planes.add(getPlaneFromResultSet(rs));
            }
        } catch (SQLException e) {
            logger.error("Cannot get planes", e);
        }
        return planes;
    }

    @Override
    public void update(Plane plane) {
        String sql = "Update Plane set model = ?, places = ? where planeId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, plane.getModel());
            stmt.setInt(2, plane.getNumbPlaces());
            stmt.setInt(3, plane.getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot update plane with id: " + plane.getId(), e);
        }
    }

    @Override
    public void delete(int planeId) {
        String sql = "Delete from Plane where planeId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, planeId);
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot delete plane with id: " + planeId, e);
        }
    }

    private Plane getPlaneFromResultSet(ResultSet resultSet) throws SQLException {
        int planeId = resultSet.getInt("planeId");
        String planeModel = resultSet.getString("model");
        int places = resultSet.getInt("places");
        return new Plane(planeId, planeModel, places);
    }
}
