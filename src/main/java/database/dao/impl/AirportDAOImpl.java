package database.dao.impl;

import database.AirportDB;
import database.dao.contract.AirportDAO;
import model.Airport;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirportDAOImpl implements AirportDAO {

    private static Logger logger = Logger.getLogger(AirportDAOImpl.class.getName());
    private Connection connection = AirportDB.getInstance().getConnection();

    @Override
    public void insert(Airport airport) {
        String sql = "Insert into Airport values (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, airport.getId());
            stmt.setString(2, airport.getName());
            stmt.setString(3, airport.getCity());
            stmt.setString(4, airport.getCountry());
            stmt.setString(5, airport.getCode());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot insert airport with id: " + airport.getId(), e);
        }
    }

    @Override
    public Airport getById(int id) {
        String sql = "Select * from Airport where apId = ?";
        Airport airport = null;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            logger.info("Executing query: " + stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                airport = getAirportFromResultSet(rs);
            }
        } catch (SQLException e) {
            logger.error("Cannot get airport with id: " + id, e);
        }
        return airport;
    }

    @Override
    public List<Airport> getAll() {
        List<Airport> airports = new ArrayList<>();
        String sql = "Select * from Airport";
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            logger.info("Executing query: " + sql);
            while (rs.next()) {
                airports.add(getAirportFromResultSet(rs));
            }
        } catch (SQLException e) {
            logger.error("Cannot get airports", e);
        }
        return airports;
    }

    @Override
    public void update(Airport airport) {
        String sql = "Update Airport set name = ?, city = ?, country = ?, code = ? where apId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, airport.getName());
            stmt.setString(2, airport.getCity());
            stmt.setString(3, airport.getCountry());
            stmt.setString(4, airport.getCode());
            stmt.setInt(5, airport.getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot update airport with id: " + airport.getId(), e);
        }
    }

    @Override
    public void delete(int apId) {
        String sql = "Delete from Airport where apId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, apId);
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot delete airport with id: " + apId, e);
        }
    }

    private Airport getAirportFromResultSet(ResultSet resultSet) throws SQLException {
        int apId = resultSet.getInt("apId");
        String name = resultSet.getString("name");
        String city = resultSet.getString("city");
        String country = resultSet.getString("country");
        String code = resultSet.getString("code");
        return new Airport(apId, name, city, country, code);
    }
}
