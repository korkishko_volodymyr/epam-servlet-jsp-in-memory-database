package database.dao.impl;

import database.AirportDB;
import database.dao.contract.TicketDAO;
import model.*;
import model.enums.ServiceClass;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TicketDAOImpl implements TicketDAO {

    private static Logger logger = Logger.getLogger(TicketDAOImpl.class.getName());
    private Connection connection = AirportDB.getInstance().getConnection();
    private AirlineDAOImpl airlineDAO = new AirlineDAOImpl();
    private AirportDAOImpl airportDAO = new AirportDAOImpl();
    private PlaneDAOImpl planeDAO = new PlaneDAOImpl();
    private PassengerDAOImpl passengerDAO = new PassengerDAOImpl();

    @Override
    public void insert(Ticket ticket) {
        String sql = "Insert into Ticket values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        passengerDAO.insert(ticket.getPassenger());
        airlineDAO.insert(ticket.getAirline());
        planeDAO.insert(ticket.getPlane());
        airportDAO.insert(ticket.getDepartureAirport());
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, ticket.getId());
            stmt.setString(2, ticket.getNumber());
            stmt.setString(3, ticket.getSource());
            stmt.setString(4, ticket.getDestination());
            stmt.setString(5, ticket.getDateOfFlight());
            stmt.setInt(6, ticket.getFlightDuration());
            stmt.setString(7, ticket.getServiceClass().toString());
            stmt.setInt(8, ticket.getPrice());
            stmt.setInt(9, ticket.getPassenger().getId());
            stmt.setInt(10, ticket.getAirline().getId());
            stmt.setInt(11, ticket.getPlane().getId());
            stmt.setInt(12, ticket.getDepartureAirport().getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot insert ticket with id: " + ticket.getId(), e);
        }
    }

    @Override
    public Ticket getById(int id) {
        String sql = "Select * from Ticket where ticketId = ?";
        Ticket ticket = null;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            logger.info("Executing query: " + stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ticket = getTicketFromResultSet(rs);
            }
        } catch (SQLException e) {
            logger.error("Cannot get ticket with id: " + id, e);
        }
        return ticket;
    }

    @Override
    public List<Ticket> getAll() {
        List<Ticket> tickets = new ArrayList<>();
        String sql = "Select * from Ticket";
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            logger.info("Executing query: " + sql);
            while (rs.next()) {
                tickets.add(getTicketFromResultSet(rs));
            }
        } catch (SQLException e) {
            logger.error("Cannot get tickets", e);
        }
        return tickets;
    }

    @Override
    public void update(Ticket ticket) {
        String sql = "Update Ticket set number = ?, source = ?, destination = ?, dateOfTravel = ?, flightDuration = ?," +
                "class = ?, price = ? where ticketId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, ticket.getNumber());
            stmt.setString(2, ticket.getSource());
            stmt.setString(3, ticket.getDestination());
            stmt.setString(4, ticket.getDateOfFlight());
            stmt.setInt(5, ticket.getFlightDuration());
            stmt.setString(6, ticket.getServiceClass().toString());
            stmt.setInt(7, ticket.getPrice());
            stmt.setInt(8, ticket.getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot update ticket with id: " + ticket.getId(), e);
        }
    }

    @Override
    public void delete(int ticketId) {
        String sql = "Delete from Ticket where ticketId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, ticketId);
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot delete ticket with id: " + ticketId, e);
        }
    }

    private Ticket getTicketFromResultSet(ResultSet resultSet) throws SQLException {
        int ticketId = resultSet.getInt("ticketId");
        String number = resultSet.getString("number");
        String source = resultSet.getString("source");
        String dest = resultSet.getString("destination");
        String date = resultSet.getString("dateOfTravel");
        int flightDuration = resultSet.getInt("flightDuration");
        ServiceClass serviceClass = ServiceClass.valueOf(resultSet.getString("class"));
        int price = resultSet.getInt("price");
        int passengerId = resultSet.getInt("pasId");
        int airId = resultSet.getInt("airId");
        int planeId = resultSet.getInt("planeId");
        int apId = resultSet.getInt("apId");
        Plane plane = planeDAO.getById(planeId);
        Passenger passenger = passengerDAO.getById(passengerId);
        Airline airline = airlineDAO.getById(airId);
        Airport airport = airportDAO.getById(apId);
        return new Ticket(ticketId, number, source, dest, date, flightDuration, serviceClass, price,
                passenger, airline, airport, plane);
    }
}
