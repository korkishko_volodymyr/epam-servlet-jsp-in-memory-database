package database.dao.impl;

import database.AirportDB;
import database.dao.contract.AirlineDAO;
import model.Airline;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirlineDAOImpl implements AirlineDAO {

    private static Logger logger = Logger.getLogger(AirlineDAOImpl.class.getName());
    private Connection connection = AirportDB.getInstance().getConnection();

    @Override
    public void insert(Airline airline) {
        String sql = "Insert into Airline values (? ,?, ?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, airline.getId());
            stmt.setString(2, airline.getName());
            stmt.setString(3, airline.getCode());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot insert airline with id: " + airline.getId(), e);
        }
    }

    @Override
    public Airline getById(int id) {
        String sql = "Select * from Airline where airId = ?";
        Airline airline = null;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            logger.info("Executing query: " + stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                airline = getAirlineFromResultSet(rs);
            }
        } catch (SQLException e) {
            logger.error("Cannot get airline with the id: " + id, e);
        }
        return airline;
    }

    @Override
    public List<Airline> getAll() {
        List<Airline> airlines = new ArrayList<>();
        String sql = "Select * from Airline";
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            logger.info("Executing query: " + sql);
            while (rs.next()) {
                airlines.add(getAirlineFromResultSet(rs));
            }
        } catch (SQLException e) {
            logger.error("Cannot get airlines", e);
        }
        return airlines;
    }

    @Override
    public void update(Airline airline) {
        String sql = "Update Airline set name = ?, code = ? where airId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, airline.getName());
            stmt.setString(2, airline.getCode());
            stmt.setInt(3, airline.getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot update airline with id: " + airline.getId(), e);
        }
    }

    @Override
    public void delete(int airId) {
        String sql = "Delete from Airline where airId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, airId);
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot delete airline with id: " + airId, e);
        }
    }

    private Airline getAirlineFromResultSet(ResultSet resultSet) throws SQLException {
        int airId = resultSet.getInt("airId");
        String name = resultSet.getString("name");
        String code = resultSet.getString("code");
        return new Airline(airId, name, code);
    }
}
