package database.dao.impl;

import database.AirportDB;
import database.dao.contract.PassengerDAO;
import model.Passenger;
import model.enums.Sex;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PassengerDAOImpl implements PassengerDAO {

    private static Logger logger = Logger.getLogger(PassengerDAOImpl.class.getName());
    private Connection connection = AirportDB.getInstance().getConnection();

    @Override
    public Passenger getById(int id) {
        String sql = "Select * from Passenger where pasId = ?";
        Passenger passenger = null;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            logger.info("Executing query: " + stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                passenger = getPassengerFromResultSet(rs);
            }
        } catch (SQLException e) {
            logger.error("Cannot get passenger with id: " + id, e);
        }
        return passenger;
    }

    @Override
    public void insert(Passenger passenger) {
        String sql = "Insert into Passenger values (? ,?, ?, ?, ?, ? ,?)";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, passenger.getId());
            stmt.setString(2, passenger.getFirstName());
            stmt.setString(3, passenger.getLastName());
            stmt.setString(4, passenger.getSex().toString());
            stmt.setInt(5, passenger.getAge());
            stmt.setString(6, passenger.getPhoneNumber());
            stmt.setString(7, passenger.getPassportNumber());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot insert passenger with id: " + passenger.getId(), e);
        }
    }

    @Override
    public List<Passenger> getAll() {
        List<Passenger> passengers = new ArrayList<>();
        String sql = "Select * from Passenger";
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            logger.info("Executing query: " + sql);
            while (rs.next()) {
                passengers.add(getPassengerFromResultSet(rs));
            }
        } catch (SQLException e) {
            logger.error("Cannot get passengers", e);
        }
        return passengers;
    }

    @Override
    public void update(Passenger passenger) {
        String sql = "Update Passenger set firstName = ?, lastName = ?, sex = ?, age = ?, phoneNumber = ?, " +
                "passport = ? where pasId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setString(1, passenger.getFirstName());
            stmt.setString(2, passenger.getLastName());
            stmt.setString(3, passenger.getSex().toString());
            stmt.setInt(4, passenger.getAge());
            stmt.setString(5, passenger.getPhoneNumber());
            stmt.setString(6, passenger.getPassportNumber());
            stmt.setInt(7, passenger.getId());
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot update passenger with id: " + passenger.getId(), e);
        }
    }

    @Override
    public void delete(int pasId) {
        String sql = "Delete from Passenger where pasId = ?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, pasId);
            logger.info("Executing query: " + stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Cannot delete passenger with id: " + pasId, e);
        }
    }

    private Passenger getPassengerFromResultSet(ResultSet resultSet) throws SQLException {
        int pasId = resultSet.getInt("pasId");
        String fName = resultSet.getString("firstName");
        String lName = resultSet.getString("lastName");
        Sex sex = Sex.valueOf(resultSet.getString("sex"));
        int age = resultSet.getInt("age");
        String phoneNumber = resultSet.getString("phoneNumber");
        String passport = resultSet.getString("passport");
        return new Passenger(pasId, fName, lName, sex, age, phoneNumber, passport);
    }
}
