package database.dao.contract;

import model.Plane;

public interface PlaneDAO extends ModelDAO<Plane> {
}
