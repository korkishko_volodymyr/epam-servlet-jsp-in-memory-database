package database.dao.contract;

import model.Passenger;

public interface PassengerDAO extends ModelDAO<Passenger> {
}
