package database.dao.contract;

import model.Airline;

public interface AirlineDAO extends ModelDAO<Airline> {
}
