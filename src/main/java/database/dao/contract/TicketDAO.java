package database.dao.contract;

import model.Ticket;

public interface TicketDAO extends ModelDAO<Ticket> {
}
