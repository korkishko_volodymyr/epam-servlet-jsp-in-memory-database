package database.dao.contract;

import java.util.List;

public interface ModelDAO<T> {

    List<T> getAll();

    T getById(int id);

    void insert(T model);

    void update(T model);

    void delete(int id);
}
