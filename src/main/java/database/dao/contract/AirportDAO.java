package database.dao.contract;

import model.Airport;

public interface AirportDAO extends ModelDAO<Airport> {
}
