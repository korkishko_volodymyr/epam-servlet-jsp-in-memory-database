package database;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class AirportDB {

    private static Logger logger = Logger.getLogger(AirportDB.class.getName());
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:mem:airport;DB_CLOSE_DELAY=-1;";
    private static final String USER = "korkishko";
    private static final String PASSWORD = "";
    private static AirportDB airportDB;
    private Connection connection;

    private AirportDB() {
        connect();
        initDatabase();
    }

    public Connection getConnection() {
        return connection;
    }

    public static AirportDB getInstance() {
        if (airportDB == null) {
            airportDB = new AirportDB();
        }
        return airportDB;
    }

    private void connect() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("Connection failed", e);
        }
        logger.info("Connection successful");
    }

    private void initDatabase() {
        final String createPlaneTable =
                "CREATE TABLE  PLANE " +
                        "(planeId int not null auto_increment, " +
                        " model VARCHAR(255), " +
                        " places INTEGER," +
                        "primary key (planeId));";
        final String createAirportTable =
                "CREATE TABLE  AIRPORT " +
                        "(apId int not null auto_increment, " +
                        " name VARCHAR(255), " +
                        " city VARCHAR(255)," +
                        " country VARCHAR(255)," +
                        " code VARCHAR(255)," +
                        "primary key (apId)); ";
        final String createAirlineTable =
                "CREATE TABLE  AIRLINE " +
                        "(airId int not null auto_increment, " +
                        " name VARCHAR(255), " +
                        " code VARCHAR(255)," +
                        "primary key (airId));";
        final String createPassengerTable =
                "CREATE TABLE  PASSENGER " +
                        "(pasId int not null auto_increment, " +
                        " firstName VARCHAR(255), " +
                        " lastName VARCHAR(255), " +
                        " sex VARCHAR(25), " +
                        " age integer , " +
                        " phoneNumber VARCHAR(255), " +
                        " passport VARCHAR(255), " +
                        "primary key (pasId));";
        final String createTicketTable =
                "CREATE TABLE TICKET " +
                        "(ticketId int not null auto_increment, " +
                        " number VARCHAR(255), " +
                        " source VARCHAR(255), " +
                        " destination VARCHAR(25), " +
                        " dateOfTravel VARCHAR(255), " +
                        " flightDuration int, " +
                        " class VARCHAR(25), " +
                        " price int, " +
                        " pasId int, " +
                        " airId int, " +
                        " planeId int, " +
                        " apId int, " +
                        "primary key (ticketId)," +
                        "foreign key (pasId) references PASSENGER(pasId) on delete cascade," +
                        "foreign key (airId) references AIRLINE(airId) on delete cascade," +
                        "foreign key (planeId) references PLANE(planeId) on delete cascade," +
                        "foreign key (apId) references AIRPORT(apId) on delete cascade);";
        final String addTestData =
                "INSERT INTO Plane VALUES " +
                        "(1,'Boeing 777', 233)," +
                        "(2,'Airbus A-310', 332)," +
                        "(3,'Boeing 747', 332)," +
                        "(4,'Antonov AN-120', 128);" +
                        "INSERT INTO Airport VALUES " +
                        "(1 ,'Lviv International Airport','Lviv', 'UA', 'LWO')," +
                        "(2 ,'Louisville International Airport','Kentucky', 'USA', 'KIA')," +
                        "(3 ,'Frankfurt Airport','Hesse', 'GER', 'FIA')," +
                        "(4 ,'Dallas/Fort Worth International Airport','Texas', 'USA', 'DIA')," +
                        "(5 ,'Indira GandhiInternational Airport','Delhi', 'IND', 'IGA');" +
                        "INSERT INTO Passenger VALUES " +
                        "(1 ,'Igor','Khomyk','MALE', 23, '0988858661', 'BK122331')," +
                        "(2 ,'Ivan','Boiko','MALE', 31, '0987788654', 'BK433212')," +
                        "(3 ,'Julia','Manko','FEMALE', 27, '0988855432', 'BН776896')," +
                        "(4 ,'Maria','Fedyk','FEMALE', 22, '0984765890', 'BН485763');" +
                        "INSERT INTO Airline VALUES " +
                        "(1 ,'Qatar Airlines','QAT')," +
                        "(2 ,'Turkish Airlines','TAA')," +
                        "(3 ,'SkyUP','SUP')," +
                        "(4 ,'Ukraine International','UIA')," +
                        "(5 ,'WizzAir','WZZ');" +
                        "INSERT INTO Ticket VALUES " +
                        "(1 ,'#3DG23','Lviv','Kyiv', '2020-04-09T12:00', 122,'BUSINESS', 1322,1,1,1,1)," +
                        "(2 ,'#FF55H','Kharkiv','Dehli', '2020-03-04T08:15', 100,'COMFORT', 566,3,2,2,5)," +
                        "(3 ,'#76JL6','Kyiv','Texas', '2020-04-13T11:56', 223,'BUSINESS', 2000,4,3,1,4)," +
                        "(4 ,'#56OIO','London','Kentucky', '2020-04-23T18:33', 332,'ECONOMY', 1322,2,4,1,2)," +
                        "(5 ,'#00L21','Rome','Lviv', '2020-04-09T22:35', 490, 'BUSINESS', 1670,3,2,2,1)";

        try (Statement stmt = connection.createStatement()) {
            logger.info("Creating table Plane");
            stmt.executeUpdate(createPlaneTable);
            logger.info("Creating table Airport");
            stmt.executeUpdate(createAirportTable);
            logger.info("Creating table Airline");
            stmt.executeUpdate(createAirlineTable);
            logger.info("Creating table Passenger");
            stmt.executeUpdate(createPassengerTable);
            logger.info("Creating table Ticket");
            stmt.executeUpdate(createTicketTable);
            logger.info("Insert test Data");
            stmt.executeUpdate(addTestData);
        } catch (SQLException e) {
            logger.error("Cannot create table/insert test data", e);
        }
    }
}