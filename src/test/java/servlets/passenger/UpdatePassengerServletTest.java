package servlets.passenger;

import model.enums.Sex;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class UpdatePassengerServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/passengerView/passengerAction.jsp";
    private static final String REDIRECT = "/passenger";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenGetPassengerToUpdate() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("sex")).thenReturn(String.valueOf(Sex.MALE));
        when(request.getParameter("age")).thenReturn("43");
        new UpdatePassengerServlet().doGet(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(response).sendRedirect(REDIRECT);
    }

    @Test
    public void whenDoPostThenGetEditPassengerPage() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new UpdatePassengerServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }
}
