package servlets.passenger;

import model.enums.Sex;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class PassengerActionServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/passengerView/passengerAction.jsp";
    private static final String REDIRECT = "/passenger";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenReturnAddNewPassengerPage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new PassengerActionServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostThenGetNewPassenger() throws IOException {
        when(request.getParameter("firstName")).thenReturn("Igor");
        when(request.getParameter("lastName")).thenReturn("Lubchenko");
        when(request.getParameter("sex")).thenReturn(Sex.MALE.toString());
        when(request.getParameter("age")).thenReturn("43");
        when(request.getParameter("phoneNumber")).thenReturn("0675544654");
        when(request.getParameter("passportNumber")).thenReturn("ВК334455");
        new PassengerActionServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("firstName");
        verify(request, atLeast(1)).getParameter("lastName");
        verify(request, atLeast(1)).getParameter("sex");
        verify(request, atLeast(1)).getParameter("age");
        verify(request, atLeast(1)).getParameter("phoneNumber");
        verify(request, atLeast(1)).getParameter("passportNumber");
        verify(response).sendRedirect(REDIRECT);
    }
}
