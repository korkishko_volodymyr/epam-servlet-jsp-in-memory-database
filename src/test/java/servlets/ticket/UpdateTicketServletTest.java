package servlets.ticket;

import model.enums.ServiceClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class UpdateTicketServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/ticketView/ticketAction.jsp";
    private static final String REDIRECT = "/ticket";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenGetPlaneToUpdate() throws IOException {
        when(request.getParameter("id")).thenReturn("3");
        when(request.getParameter("flightDuration")).thenReturn("125");
        when(request.getParameter("serviceClass")).thenReturn(ServiceClass.BUSINESS.toString());
        when(request.getParameter("price")).thenReturn("1600");
        new UpdateTicketServlet().doGet(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(response).sendRedirect(REDIRECT);
    }

    @Test
    public void whenDoPostThenGetEditPlanePage() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new UpdateTicketServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }
}
