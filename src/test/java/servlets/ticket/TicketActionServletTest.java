package servlets.ticket;

import model.enums.ServiceClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class TicketActionServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/ticketView/ticketAction.jsp";
    private static final String REDIRECT = "/ticket";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenReturnAddNewTicketPage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new TicketActionServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostThenGetNewTicket() throws IOException {
        when(request.getParameter("number")).thenReturn("3E213T");
        when(request.getParameter("source")).thenReturn("Paris");
        when(request.getParameter("destination")).thenReturn("London");
        when(request.getParameter("dateOfFlight")).thenReturn("2020-04-19T20:35");
        when(request.getParameter("flightDuration")).thenReturn("120");
        when(request.getParameter("serviceClass")).thenReturn(ServiceClass.ECONOMY.toString());
        when(request.getParameter("price")).thenReturn("1500");
        when(request.getParameter("passenger")).thenReturn("3");
        when(request.getParameter("airline")).thenReturn("1");
        when(request.getParameter("plane")).thenReturn("4");
        when(request.getParameter("airport")).thenReturn("1");
        new TicketActionServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("number");
        verify(request, atLeast(1)).getParameter("source");
        verify(request, atLeast(1)).getParameter("destination");
        verify(request, atLeast(1)).getParameter("dateOfFlight");
        verify(request, atLeast(1)).getParameter("flightDuration");
        verify(request, atLeast(1)).getParameter("serviceClass");
        verify(request, atLeast(1)).getParameter("price");
        verify(response).sendRedirect(REDIRECT);
    }
}
