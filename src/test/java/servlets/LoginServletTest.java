package servlets;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class LoginServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/login.jsp";
    private static final String REDIRECT = "/ticket";
    private static final String LOGIN = "korkishko1998";
    private static final String PASSWORD = "epamLviv";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenReturnLoginPage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new LoginServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostWithValidLogin() throws IOException, ServletException {
        when(request.getParameter("login")).thenReturn(LOGIN);
        when(request.getParameter("password")).thenReturn(PASSWORD);
        new LoginServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("login");
        verify(request, atLeast(1)).getParameter("password");
        verify(response).sendRedirect(REDIRECT);
    }

    @Test
    public void whenDoPostWithWrongLogin() throws IOException, ServletException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        when(request.getParameter("login")).thenReturn("wrongLogin");
        when(request.getParameter("password")).thenReturn("wrongPassword");
        new LoginServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("login");
        verify(request, atLeast(1)).getParameter("password");
        verify(dispatcher).forward(request, response);
    }
}
