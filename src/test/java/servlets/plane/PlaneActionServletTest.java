package servlets.plane;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class PlaneActionServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/planeView/planeAction.jsp";
    private static final String REDIRECT = "/plane";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenReturnAddNewPlanePage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new PlaneActionServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostThenGetNewPlane() throws IOException {
        when(request.getParameter("planeModel")).thenReturn("Super Jet");
        when(request.getParameter("planePlaces")).thenReturn("10");
        new PlaneActionServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("planeModel");
        verify(request, atLeast(1)).getParameter("planePlaces");
        verify(response).sendRedirect(REDIRECT);
    }
}
