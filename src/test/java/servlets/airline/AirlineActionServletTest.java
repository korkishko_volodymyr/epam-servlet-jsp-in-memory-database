package servlets.airline;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class AirlineActionServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/airlineView/airlineAction.jsp";
    private static final String REDIRECT = "/airline";

    @Before
    public void init() { MockitoAnnotations.initMocks(this); }

    @Test
    public void whenDoGetThenReturnAddNewAirlinePage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new AirlineActionServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostThenGetNewAirline() throws IOException {
        when(request.getParameter("airlineName")).thenReturn("Ryanair");
        when(request.getParameter("airlineCode")).thenReturn("RAA");
        new AirlineActionServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("airlineName");
        verify(request, atLeast(1)).getParameter("airlineCode");
        verify(response).sendRedirect(REDIRECT);
    }
}
