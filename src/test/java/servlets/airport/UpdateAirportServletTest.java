package servlets.airport;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class UpdateAirportServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/airportView/airportAction.jsp";
    private static final String REDIRECT = "/airport";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenGetAirportToUpdate() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        new UpdateAirportServlet().doGet(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(response).sendRedirect(REDIRECT);
    }

    @Test
    public void whenDoPostThenGetEditAirportPage() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new UpdateAirportServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("id");
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }
}
