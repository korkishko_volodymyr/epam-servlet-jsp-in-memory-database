package servlets.airport;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class AirportActionServletTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher dispatcher;

    private static final String PATH = "/view/airportView/airportAction.jsp";
    private static final String REDIRECT = "/airport";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenDoGetThenReturnAddNewAirportPage() throws ServletException, IOException {
        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);
        new AirportActionServlet().doGet(request, response);
        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void whenDoPostThenGetNewAirport() throws IOException {
        when(request.getParameter("airportName")).thenReturn("Odessa Airport");
        when(request.getParameter("airportCode")).thenReturn("ODA");
        when(request.getParameter("airportCountry")).thenReturn("UA");
        when(request.getParameter("airportCity")).thenReturn("Odessa");
        new AirportActionServlet().doPost(request, response);
        verify(request, atLeast(1)).getParameter("airportName");
        verify(request, atLeast(1)).getParameter("airportCode");
        verify(request, atLeast(1)).getParameter("airportCountry");
        verify(request, atLeast(1)).getParameter("airportCity");
        verify(response).sendRedirect(REDIRECT);
    }
}
