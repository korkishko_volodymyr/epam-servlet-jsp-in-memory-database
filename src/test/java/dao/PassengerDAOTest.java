package dao;

import database.dao.contract.PassengerDAO;
import database.dao.impl.PassengerDAOImpl;
import model.Passenger;
import model.enums.Sex;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PassengerDAOTest {

    private PassengerDAO passengerDAO;
    private Passenger passenger;

    @Before
    public void init() {
        passengerDAO = new PassengerDAOImpl();
        passenger = new Passenger("Igor","Lubchenko", Sex.MALE,43,
                "0675544654","ВК334455");
    }

    @Test
    public void testInsert() {
        passengerDAO.insert(passenger);
        Passenger inserted = passengerDAO.getById(passenger.getId());
        assertNotNull(inserted);
        assertEquals(passenger.getId(), inserted.getId());
        assertEquals(passenger.getFirstName(), inserted.getFirstName());
        assertEquals(passenger.getLastName(), inserted.getLastName());
        assertEquals(passenger.getAge(), inserted.getAge());
        assertEquals(passenger.getPassportNumber(), inserted.getPassportNumber());
        assertEquals(passenger.getPhoneNumber(), inserted.getPhoneNumber());
        assertEquals(passenger.getSex(), inserted.getSex());
    }

    @Test
    public void testGetById() {
        final int id = 5;
        Passenger received = passengerDAO.getById(id);
        assertNull(received);
        passengerDAO.insert(passenger);
        received = passengerDAO.getById(id);
        assertEquals(passenger.getId(), received.getId());
        assertEquals(passenger.getFirstName(), received.getFirstName());
        assertEquals(passenger.getLastName(), received.getLastName());
        assertEquals(passenger.getAge(), received.getAge());
        assertEquals(passenger.getPassportNumber(), received.getPassportNumber());
        assertEquals(passenger.getPhoneNumber(), received.getPhoneNumber());
        assertEquals(passenger.getSex(), received.getSex());
    }

    @Test
    public void testUpdate() {
        final int id = 4;
        passenger = passengerDAO.getById(id);
        passenger.setLastName("Boiko");
        passenger.setAge(44);
        passenger.setPhoneNumber("0937744123");
        passengerDAO.update(passenger);
        Passenger updatedPassenger = passengerDAO.getById(id);
        assertEquals(passenger.getId(), updatedPassenger.getId());
        assertEquals(passenger.getFirstName(), updatedPassenger.getFirstName());
        assertEquals(passenger.getLastName(), updatedPassenger.getLastName());
        assertEquals(passenger.getAge(), updatedPassenger.getAge());
        assertEquals(passenger.getPassportNumber(), updatedPassenger.getPassportNumber());
        assertEquals(passenger.getPhoneNumber(), updatedPassenger.getPhoneNumber());
        assertEquals(passenger.getSex(), updatedPassenger.getSex());
    }

    @Test
    public void testDelete() {
        final int id = 2;
        passengerDAO.delete(id);
        boolean isDeleted = passengerDAO.getAll().stream().anyMatch(passenger -> passenger.getId() == id);
        assertFalse(isDeleted);
        assertNull(passengerDAO.getById(id));
    }

    @Test
    public void testGetAll() {
        final int expectedSize = 4;
        List<Passenger> passengers = passengerDAO.getAll();
        assertNotNull(passengers);
        assertEquals(passengers.size(), 4);
    }
}
