package dao;

import database.dao.contract.*;
import database.dao.impl.*;
import model.*;
import model.enums.ServiceClass;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TicketDAOTest {

    private TicketDAO ticketDAO;
    private PlaneDAO planeDAO;
    private Ticket ticket;

    @Before
    public void init() {
        PassengerDAO passengerDAO = new PassengerDAOImpl();
        AirportDAO airportDAO = new AirportDAOImpl();
        AirlineDAO airlineDAO = new AirlineDAOImpl();
        ticketDAO = new TicketDAOImpl();
        planeDAO = new PlaneDAOImpl();
        ticket = new Ticket("#3E213T", "Paris", "London", "2020-04-19T20:35",
                120, ServiceClass.ECONOMY, 1500, passengerDAO.getById(1),
                airlineDAO.getById(3), airportDAO.getById(1), planeDAO.getById(4));
    }

    @Test
    public void testInsert() {
        ticketDAO.insert(ticket);
        Ticket inserted = ticketDAO.getById(ticket.getId());
        assertNotNull(inserted);
        assertEquals(ticket.getId(), inserted.getId());
        assertEquals(ticket.getNumber(), inserted.getNumber());
        assertEquals(ticket.getFlightDuration(), inserted.getFlightDuration());
        assertEquals(ticket.getPrice(), inserted.getPrice());
        assertEquals(ticket.getServiceClass(), inserted.getServiceClass());
        assertEquals(ticket.getDateOfFlight(), inserted.getDateOfFlight());
        assertEquals(ticket.getSource(), inserted.getSource());
        assertEquals(ticket.getDestination(), inserted.getDestination());
        assertEquals(ticket.getAirline(), inserted.getAirline());
        assertEquals(ticket.getPlane(), inserted.getPlane());
        assertEquals(ticket.getDepartureAirport(), inserted.getDepartureAirport());
        assertEquals(ticket.getPassenger(), inserted.getPassenger());
    }

    @Test
    public void testGetById() {
        final int id = 6;
        Ticket received = ticketDAO.getById(id);
        assertNull(received);
        ticketDAO.insert(ticket);
        received = ticketDAO.getById(id);
        assertEquals(ticket.getId(), received.getId());
        assertEquals(ticket.getNumber(), received.getNumber());
        assertEquals(ticket.getFlightDuration(), received.getFlightDuration());
        assertEquals(ticket.getPrice(), received.getPrice());
        assertEquals(ticket.getServiceClass(), received.getServiceClass());
        assertEquals(ticket.getDateOfFlight(), received.getDateOfFlight());
        assertEquals(ticket.getSource(), received.getSource());
        assertEquals(ticket.getDestination(), received.getDestination());
        assertEquals(ticket.getAirline(), received.getAirline());
        assertEquals(ticket.getPlane(), received.getPlane());
        assertEquals(ticket.getDepartureAirport(), received.getDepartureAirport());
        assertEquals(ticket.getPassenger(), received.getPassenger());
    }

    @Test
    public void testUpdate() {
        final int id = 2;
        ticket = ticketDAO.getById(id);
        ticket.setPlane(planeDAO.getById(id));
        ticket.setPrice(1700);
        ticket.setServiceClass(ServiceClass.COMFORT);
        ticketDAO.update(ticket);
        Ticket updatedTicket = ticketDAO.getById(id);
        assertEquals(ticket.getId(), updatedTicket.getId());
        assertEquals(ticket.getNumber(), updatedTicket.getNumber());
        assertEquals(ticket.getFlightDuration(), updatedTicket.getFlightDuration());
        assertEquals(ticket.getPrice(), updatedTicket.getPrice());
        assertEquals(ticket.getServiceClass(), updatedTicket.getServiceClass());
        assertEquals(ticket.getDateOfFlight(), updatedTicket.getDateOfFlight());
        assertEquals(ticket.getSource(), updatedTicket.getSource());
        assertEquals(ticket.getDestination(), updatedTicket.getDestination());
        assertEquals(ticket.getAirline(), updatedTicket.getAirline());
        assertEquals(ticket.getPlane(), updatedTicket.getPlane());
        assertEquals(ticket.getDepartureAirport(), updatedTicket.getDepartureAirport());
        assertEquals(ticket.getPassenger(), updatedTicket.getPassenger());
    }

    @Test
    public void testDelete() {
        final int id = 3;
        ticketDAO.delete(id);
        boolean isDeleted = ticketDAO.getAll().stream().anyMatch(ticket -> ticket.getId() == id);
        assertFalse(isDeleted);
        assertNull(ticketDAO.getById(id));
    }

    @Test
    public void testGetAll() {
        final int expectedSize = 4;
        List<Ticket> tickets = ticketDAO.getAll();
        assertNotNull(tickets);
        assertEquals(tickets.size(), 4);
    }
}
