package dao;

import database.dao.contract.PlaneDAO;
import database.dao.impl.PlaneDAOImpl;
import model.Plane;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PlaneDAOTest {

    private PlaneDAO planeDAO;
    private Plane plane;

    @Before
    public void init(){
        planeDAO = new PlaneDAOImpl();
        plane = new Plane("Super Jet", 10);
    }

    @Test
    public void testInsert() {
        planeDAO.insert(plane);
        Plane inserted = planeDAO.getById(plane.getId());
        assertNotNull(inserted);
        assertEquals(plane.getId(), inserted.getId());
        assertEquals(plane.getModel(), inserted.getModel());
        assertEquals(plane.getNumbPlaces(), inserted.getNumbPlaces());
    }

    @Test
    public void testGetById() {
        final int id = 7;
        Plane received = planeDAO.getById(id);
        assertNull(received);
        planeDAO.insert(plane);
        received = planeDAO.getById(id);
        assertEquals(plane.getId(), received.getId());
        assertEquals(plane.getModel(), received.getModel());
        assertEquals(plane.getNumbPlaces(), received.getNumbPlaces());
    }

    @Test
    public void testUpdate() {
        final int id = 2;
        plane = planeDAO.getById(id);
        plane.setNumbPlaces(12);
        planeDAO.update(plane);
        Plane updatedPlane = planeDAO.getById(id);
        assertEquals(plane.getId(), updatedPlane.getId());
        assertEquals(plane.getModel(), updatedPlane.getModel());
        assertEquals(plane.getNumbPlaces(), updatedPlane.getNumbPlaces());
    }

    @Test
    public void testDelete() {
        final int id = 1;
        planeDAO.delete(id);
        boolean isDeleted = planeDAO.getAll().stream().anyMatch(plane -> plane.getId() == id);
        assertFalse(isDeleted);
        assertNull(planeDAO.getById(id));
    }

    @Test
    public void testGetAll() {
        final int expectedSize = 4;
        List<Plane> planes = planeDAO.getAll();
        assertNotNull(planes);
        assertEquals(planes.size(), 4);
    }
}
