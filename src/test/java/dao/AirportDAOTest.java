package dao;

import database.dao.contract.AirportDAO;
import database.dao.impl.AirportDAOImpl;
import model.Airport;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AirportDAOTest {

    private AirportDAO airportDAO;
    private Airport airport;

    @Before
    public void init() {
        airportDAO = new AirportDAOImpl();
        airport = new Airport("Odessa Airport", "Odessa", "UA", "ODA");
    }

    @Test
    public void testInsert() {
        airportDAO.insert(airport);
        Airport inserted = airportDAO.getById(airport.getId());
        assertNotNull(inserted);
        assertEquals(airport.getId(), inserted.getId());
        assertEquals(airport.getName(), inserted.getName());
        assertEquals(airport.getCode(), inserted.getCode());
        assertEquals(airport.getCountry(), inserted.getCountry());
        assertEquals(airport.getCity(), inserted.getCity());
    }

    @Test
    public void testGetById() {
        final int id = 8;
        Airport received = airportDAO.getById(id);
        assertNull(received);
        airportDAO.insert(airport);
        received = airportDAO.getById(id);
        assertEquals(airport.getId(), received.getId());
        assertEquals(airport.getName(), received.getName());
        assertEquals(airport.getCode(), received.getCode());
        assertEquals(airport.getCountry(), received.getCountry());
        assertEquals(airport.getCity(), received.getCity());
    }

    @Test
    public void testUpdate() {
        final int id = 2;
        airport = airportDAO.getById(id);
        airport.setName("Odessa International Airport");
        airportDAO.update(airport);
        Airport updatedAirport = airportDAO.getById(id);
        assertEquals(updatedAirport.getId(), airport.getId());
        assertEquals(updatedAirport.getName(), airport.getName());
        assertEquals(updatedAirport.getCode(), airport.getCode());
        assertEquals(updatedAirport.getCountry(), airport.getCountry());
        assertEquals(updatedAirport.getCity(), airport.getCity());
    }

    @Test
    public void testDelete() {
        final int id = 1;
        airportDAO.delete(id);
        boolean isDeleted = airportDAO.getAll().stream().anyMatch(airport -> airport.getId() == id);
        assertFalse(isDeleted);
        assertNull(airportDAO.getById(id));
    }

    @Test
    public void testGetAll() {
        final int expectedSize = 5;
        List<Airport> airports = airportDAO.getAll();
        assertNotNull(airports);
        assertEquals(airports.size(), expectedSize);
    }
}
