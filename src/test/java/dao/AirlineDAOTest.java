package dao;

import database.dao.contract.AirlineDAO;
import database.dao.impl.AirlineDAOImpl;
import model.Airline;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AirlineDAOTest {

    private AirlineDAO airlineDAO;
    private Airline airline;

    @Before
    public void init(){
        airlineDAO = new AirlineDAOImpl();
        airline = new Airline("Ryanair", "RAA");
    }

    @Test
    public void testInsert() {
        airlineDAO.insert(airline);
        Airline inserted = airlineDAO.getById(airline.getId());
        assertNotNull(inserted);
        assertEquals(airline.getId(), inserted.getId());
        assertEquals(airline.getName(), inserted.getName());
        assertEquals(airline.getCode(), inserted.getCode());
    }

    @Test
    public void testGetById() {
        final int id = 8;
        Airline received = airlineDAO.getById(id);
        assertNull(received);
        airlineDAO.insert(airline);
        received = airlineDAO.getById(id);
        assertEquals(airline.getId(), received.getId());
        assertEquals(airline.getName(), received.getName());
        assertEquals(airline.getCode(), received.getCode());
    }

    @Test
    public void testUpdate() {
        final int id = 3;
        airline = airlineDAO.getById(id);
        airline.setName("Turkish");
        airlineDAO.update(airline);
        Airline updatedAirline = airlineDAO.getById(id);
        assertEquals(updatedAirline.getId(), airline.getId());
        assertEquals(updatedAirline.getName(), airline.getName());
        assertEquals(updatedAirline.getCode(), airline.getCode());
    }

    @Test
    public void testDelete() {
        final int id = 5;
        airlineDAO.delete(id);
        boolean isDeleted = airlineDAO.getAll().stream().anyMatch(airline -> airline.getId() == id);
        assertFalse(isDeleted);
        assertNull(airlineDAO.getById(id));
    }

    @Test
    public void testGetAll() {
        final int expectedSize = 5;
        List<Airline> airlines = airlineDAO.getAll();
        assertNotNull(airlines);
        assertEquals(airlines.size(), expectedSize);
    }
}
